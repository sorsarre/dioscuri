# DefaultModels.R
# Contains model definitions for default models

# Import model class
source('Model.R')

# Function that loads model code from the file specified
loadModel <- function(file.name)
{
  z <- new.env()
  source(file.name, local=z)
  model.name = get('Dioscuri.ModelName', envir=z)
  model.fitter = get('Dioscuri.ModelFitter', envir=z)
  model.printer = get('Dioscuri.ModelPrinter', envir=z)
  if (is.null(model.name) || is.null(model.fitter) || is.null(model.printer))
    stop(paste(file.name, '-- wrong model file, missing entities.'))
  ret <- new("Model", model.name, model.fitter, model.printer)
  return(ret)
}

# Lists all available model files
listModelFiles <- function()
{
  return(as.list(list.files('Models/', '*.R', full.names=T)))
}

# Now, load all available models
global_DefaultModels <- as.list(sapply(listModelFiles(), loadModel))
names(global_DefaultModels) <- sapply(global_DefaultModels, function(m) { m@name })

listModels <- function() names(global_DefaultModels)
                                  
# getModel function
getModel <- function(name)
{
  present = any(names(global_DefaultModels) == name)
  if (!present)
    stop(paste("Model", name, "doesn\'t exist."))
  else
    return(global_DefaultModels[name][[1]])
}